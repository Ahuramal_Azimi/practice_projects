package com.amirmohammadazimi.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button next, previous;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();
        MyListener listener = new MyListener(fragmentManager);

        next = (Button) findViewById(R.id.buttonNext);
        next.setOnClickListener(listener);

        previous = (Button) findViewById(R.id.buttonPrevious);
        previous.setOnClickListener(listener);
    }
}
