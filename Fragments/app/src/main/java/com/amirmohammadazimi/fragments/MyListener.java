package com.amirmohammadazimi.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;

/**
 * Created by Amir Mohammad Azimi on 4/10/2017.
 */

public class MyListener implements View.OnClickListener {

    private FragmentManager fragmentManager;
    int i = 1;

    public MyListener(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonPrevious) {
            if (i > 1) {
                i--;
            } else {
                i = 10;
            }
        } else if (v.getId() == R.id.buttonNext) {
            if (i < 10) {
                i++;
            } else {
                i = 1;
            }
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = null;
        switch (i) {
            case 1:
                fragment = new firstFragment();
                break;
            case 2:
                fragment = new secondFragment();
                break;
            case 3:
                fragment = new thirdFragment();
                break;
            case 4:
                fragment = new fourthFragment();
                break;
            case 5:
                fragment = new fifthFragment();
                break;
            case 6:
                fragment = new sixthFragment();
                break;
            case 7:
                fragment = new seventhFragment();
                break;
            case 8:
                fragment = new eighthFragment();
                break;
            case 9:
                fragment = new ninthFragment();
                break;
            case 10:
                fragment = new tenthFragment();
                break;
        }

        fragmentTransaction.add(R.id.container, fragment);
        fragmentTransaction.commit();


    }
}
